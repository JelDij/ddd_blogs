import domain.Entities.BlogPost;
import domain.Entities.User;

public class Main {

    public static void main(String[] args) {
        User author = new User("Jelmer", true);
        User nonAuthor = new User("Piet", false);
        String validContent = "Dit is een test. Dit is een test. Dit is een test. Dit is een test. Dit is een test.Dit is een test.Dit is een test.";
        BlogPost firstPost = new BlogPost("test", validContent, author);
        System.out.println(firstPost);

    }
}
