package domain.Entities;

public class Comment {

    private User commenter;
    private String comment;
    //should we register here which blogpost is commented on? I assume this would be redundant because this is done in blogpost

    public Comment(User commenter, String comment) {
        this.setCommenter(commenter);
        this.setComment(comment);
    }

    private void setCommenter(User commenter) {
        this.commenter = commenter;
    }

    private void setComment(String comment) {
        if (this.comment.length() < 10) {
            throw new RuntimeException("Comment must be at least 10 characters long");
        }
        this.comment = comment;
    }

    public User getCommenter() {
        return commenter;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commenter=" + commenter +
                ", comment='" + comment + '\'' +
                '}';
    }
}
