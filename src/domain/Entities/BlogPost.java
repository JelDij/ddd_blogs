package domain.Entities;

import java.util.ArrayList;
import java.util.List;

public class BlogPost {

    private String title;
    private String content;
    private User author;
    private boolean published;
    private List<Comment> comments;
    private List<User> likedBy;

    public BlogPost(String title, String content, User author) {
        this.setTitle(title);
        this.setContent(content);
        this.setAuthor(author);
        this.published = false;
        this.comments = new ArrayList<>();
        this.likedBy = new ArrayList<>();
    }

    public void setTitle(String title) {
        if (this.published) {
            throw new RuntimeException("Published post cannot be changed");
        }
        if (title.isEmpty()) {
            throw new RuntimeException("Can't set empty title");
        }
        this.title = title;
    }

    public void setAuthor(User author) {
        if (this.author != null) {
            throw new RuntimeException("Author cannot be changed");
        }
        if (author.isCanPublish()) {
            this.author = author;
        } else {
            throw new RuntimeException("User not allowed to create blog post");
        }
    }

    public void setContent(String content) {
        if (this.published) {
            throw new RuntimeException("Published post cannot be changed");
        }
        if (content.length() < 100) {
            throw new RuntimeException("Content must be at least 100 characters in length");
        }
        this.content = content;
    }

    public void publish() {
        if (this.title.isEmpty() || this.content.isEmpty()) {
            throw new RuntimeException("Post must contain a title and content to be published");
        }
        if (!this.likedBy.isEmpty() && !this.comments.isEmpty()) {
            throw new RuntimeException("Post can't be unpublished because it has comments and/or is liked by users");
        }
        this.published = true;
    }

    public void unpublish() {
        if (!this.comments.isEmpty() || !this.likedBy.isEmpty()) {
            throw new RuntimeException("Post can't be unpublished because it has comments and/or is liked by users");
        }
        this.published = false;
    }

    public void addComment(Comment comment) {
        if (this.published) {
            this.comments.add(comment);
        } else {
            throw new RuntimeException("Unpublished post cannot be commented");
        }
    }

    public void addLike(User likedBy) {
        if (this.published) {
            this.likedBy.add(likedBy);
        } else {
            throw new RuntimeException("Unpublished post cannot be liked");
        }
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public User getAuthor() {
        return author;
    }

    public boolean isPublished() {
        return published;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<User> getLikedBy() {
        return likedBy;
    }

    @Override
    public String toString() {
        return "BlogPost{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", author=" + author +
                ", published=" + published +
                ", comments=" + comments +
                ", likedBy=" + likedBy +
                '}';
    }
}
