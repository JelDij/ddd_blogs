package domain.Entities;

public class User {

    private String username;
    private boolean canPublish;
    // should the author entity have a list with create blog posts? This is kinda redundant because we have this information in blogpost

    public User(String username, boolean canPublish) {
        this.setUsername(username);
        this.setCanPublish(canPublish);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if (username.isEmpty()) {
            throw new RuntimeException("Username is mandatory");
        }
        // could add logic to check for unique username but then we need to add repository with list of usernames
        this.username = username;
    }

    public boolean isCanPublish() {
        return canPublish;
    }

    public void setCanPublish(boolean canPublish) {
        this.canPublish = canPublish;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", canPublish=" + canPublish +
                '}';
    }
}
